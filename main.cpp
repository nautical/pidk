#include <stdio.h>
#include <iostream>
#include <string>
#include <curl/curl.h>
#include "pidk.h"
#include "anyoption.h"
#include "color.h"
#include "deps/libgit2/git2.h"
#include "deps/libgit2/git2/clone.h"
#include <pthread.h>
#include <unistd.h>
#include "deps/sqlite/sqlite3.h"
#include "config.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

//copy the libgit to /usr/lib/

#define PRINT "zu"
using namespace std;

string data_buffer;
string get_home_dir();
string __home__ = get_home_dir();

size_t curl_write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
    data_buffer.append((char*) ptr);
    return size*nmemb;
}

typedef struct progress_data {
    git_transfer_progress fetch_progress;
    size_t completed_steps;
    size_t total_steps;
    const char *path;
} progress_data;

static void print_progress(const progress_data *pd) {
    int network_percent = (100 * pd->fetch_progress.received_objects) / pd->fetch_progress.total_objects;
    cout << "\r" << network_percent << " %" << flush;
}

static int fetch_progress(const git_transfer_progress *stats, void *payload) {
    progress_data *pd = (progress_data*) payload;
    pd->fetch_progress = *stats;
    print_progress(pd);
    return 0;
}

static void checkout_progress(const char *path, size_t cur, size_t tot, void *payload) {
    progress_data *pd = (progress_data*) payload;
    pd->completed_steps = cur;
    pd->total_steps = tot;
    pd->path = path;
    print_progress(pd);
}

string get_git_url(char* param) {
    CURL *curl_handle;
    CURLcode res;
    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    string url = _Api_url;
    url += param;
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, curl_write_data);
    curl_easy_perform(curl_handle);
    curl_easy_cleanup(curl_handle);
    return data_buffer;
}

int do_clone(git_repository *repo, string url, string path) {
    progress_data pd = {
        {0}
    };
    git_repository *cloned_repo = NULL;
    git_clone_options clone_opts = GIT_CLONE_OPTIONS_INIT;
    git_checkout_opts checkout_opts = GIT_CHECKOUT_OPTS_INIT;
    int error;
    (void) repo;
    checkout_opts.checkout_strategy = GIT_CHECKOUT_SAFE_CREATE;
    checkout_opts.progress_cb = checkout_progress;
    checkout_opts.progress_payload = &pd;
    clone_opts.checkout_opts = checkout_opts;
    clone_opts.fetch_progress_cb = &fetch_progress;
    clone_opts.fetch_progress_payload = &pd;
    cout << green << "[downloading]" << def << endl;
    error = git_clone(&cloned_repo, url.c_str(), path.c_str(), &clone_opts);
    if (error != 0) {
        const git_error *err = giterr_last();
        if (err) {
            cout << red << "[ERROR] " << def << err->message << endl;
        } else {
            cout << red << "[ERROR] " << def << "No detailed info" << endl;
        }
    } else if (cloned_repo) git_repository_free(cloned_repo);
    return error;
}

bool clone(string url, char *name) {
    int error;
    git_repository *repo;
    error = git_repository_open(&repo, ".git");
    if (error < 0)
        repo = NULL;
    string location(__home__ + _downloads);
    location += name;
    error = do_clone(repo, url.c_str(), location);
    if (error < 0) return false;
    if (repo) git_repository_free(repo);
    return true;
}

static int DBcallback(void *NotUsed, int argc, char **argv, char **azColName) {
    int i;
    for (i = 0; i < argc; i++) {
        cout << blue << azColName[i] << " " << def << (argv[i] ? argv[i] : "NULL") << endl;
    }
    printf("\n");
    return 0;
}

void Create_Fresh_DB() {
    sqlite3 *db;
    char *sql;
    char *zErrMsg = 0;
    int rc;
    rc = sqlite3_open((__home__ + _vim_db).c_str(), &db);
    if (rc) {
        cout << red << "[ERROR] " << def << "Can't open database" << endl;
        exit(0);
    } else {
        sql = "CREATE TABLE VIM_PLUGINS("  \
         "module           TEXT    NOT NULL," \
         "location            TEXT     NOT NULL);";
        rc = sqlite3_exec(db, sql, DBcallback, 0, &zErrMsg);
        if (rc != SQLITE_OK) {
            cout << red << "[ERROR] " << "DB error : " << def << zErrMsg << endl;
            sqlite3_free(zErrMsg);
        } else {
            cout << green << "[SUCCESS] " << def << "DB created successfully" << endl;
        }
        sqlite3_close(db);
    }
}

void insert_into_db(const char *db_file, char *name) {
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    string sql;
    rc = sqlite3_open(db_file, &db);
    if (rc) {
        cout << red << "[ERROR] " << def << "Can't open database" << endl;
        exit(0);
    } else {
        string location(__home__ + _downloads);
        location += name;
        string _name(name);
        sql = "INSERT INTO VIM_PLUGINS(module,location) VALUES ('" + _name + "','" + location + "' );";
        rc = sqlite3_exec(db, sql.c_str(), DBcallback, 0, &zErrMsg);
        if (rc != SQLITE_OK) {
            cout << red << "[ERROR] " << def << "DB error : " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
        } else {
            cout << green << "[SUCCESS] " << def << "VPM updated" << endl;
        }
        sqlite3_close(db);
    }
}

static int query_callback(void *data, int argc, char **argv, char **azColName) {
    int i;
    cout << "----" << endl;
    for (i = 0; i < argc; i++) {
        cout << blue << azColName[i] << " " << def << (argv[i] ? argv[i] : "NULL") << endl;
    }
    return 0;
}

void query_db() {
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    char *sql;
    rc = sqlite3_open((__home__ + _vim_db).c_str(), &db);
    if (rc) {
        cout << red << "[ERROR] " << def << "Can't open database" << endl;
        exit(0);
    } else {
        sql = "SELECT * from VIM_PLUGINS";
        rc = sqlite3_exec(db, sql, query_callback, 0, &zErrMsg);
        if (rc != SQLITE_OK) {
            cout << red << "[ERROR] " << def << "DB error : " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
        } else {

        }
        sqlite3_close(db);
    }
}

bool query_if_exists(string plugin) {
    sqlite3 *db;
    sqlite3_stmt *stmt;
    int rc;
    string sql;
    rc = sqlite3_open((__home__ + _vim_db).c_str(), &db);
    if (rc) {
        cout << red << "[ERROR] " << def << "Can't open database" << endl;
        exit(0);
    } else {
        sql = "SELECT location from VIM_PLUGINS WHERE module='" + plugin + "'";
        rc = sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, 0);
        if (rc) {
            cout << red << "[ERROR] " << def << "DB error " << endl;
        } else {
            if (sqlite3_step(stmt) == SQLITE_ROW) {
                if (sqlite3_column_count(stmt) > 0) {
                    sqlite3_finalize(stmt);
                    sqlite3_close(db);
                    return true;
                } else {
                    sqlite3_finalize(stmt);
                    sqlite3_close(db);
                    return false;
                }
            }
        }
        sqlite3_close(db);
        return false;
    }
}

static int remove_callbk(void *data, int argc, char **argv, char **azColName) {
    int i;
    for (i = 0; i < argc; i++) {
        string path("rm -fr ");
        path += argv[i];
        system(path.c_str());
    }
    return 0;
}

void remove_plugin(string name) {
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    string sql;
    rc = sqlite3_open((__home__ + _vim_db).c_str(), &db);
    if (rc) {
        cout << red << "[ERROR] " << def << "Can't open database" << endl;
        exit(0);
    } else {
        sql = "SELECT location from VIM_PLUGINS WHERE module='" + name + "'";
        rc = sqlite3_exec(db, sql.c_str(), remove_callbk, 0, &zErrMsg);
        if (rc != SQLITE_OK) {
            cout << red << "[ERROR] " << def << "DB error : " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
        } else {
            sql = "DELETE from VIM_PLUGINS WHERE module='" + name + "'";
            rc = sqlite3_exec(db, sql.c_str(), remove_callbk, 0, &zErrMsg);
            if (rc != SQLITE_OK) {
                cout << red << "[ERROR] " << def << "DB error : " << zErrMsg << endl;
                sqlite3_free(zErrMsg);
            } else {
                cout << green << "[SUCCESS] " << def << "Removed Successfully" << endl;
            }
        }
        sqlite3_close(db);
    }
}

string get_home_dir() {
    struct passwd *pw = getpwuid(getuid());
    string home(pw->pw_dir);
    home += "/";
    return home;
}

void list_dir() {
    Ndir_dir dir;
    if (Ndir_open_sorted(&dir, (__home__ + _downloads).substr(0, (__home__ + _downloads).length() - 1).c_str()) == -1) {
        cout << red << "[ERROR] " << def << "Error listing bundles" << endl;
        Ndir_close(&dir);
        return;
    }
    int i;
    char input[256];
    for (i = 0; i < dir.n_files; i++) {
        Ndir_file file;
        if (Ndir_readfile_n(&dir, &file, i) == -1) {
            cout << red << "[ERROR] " << def << "Error listing bundles" << endl;
            Ndir_close(&dir);
            return;
        }
        if (file.is_dir) {
            if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0)) {
                string path(file.name);
                if (!query_if_exists(path))
                    insert_into_db((__home__ + _vim_db).c_str(), file.name);
                //cout << file.name << " :" << file.path << endl;
            }
        }
    }
    Ndir_close(&dir);
    cout << green << "[SUCCESS] " << def << "Synced Successfully" << endl;
}

int main(int argc, char* argv[]) {
    ifstream my_file((__home__ + _vim_db).c_str());
    if (my_file.good()) {
    } else {
        Create_Fresh_DB();
    }
    AnyOption *opt = new AnyOption();
    opt->addUsage("Vpm ( Vim plugin manager )");
    opt->addUsage("Usage: ");
    opt->addUsage("");
    opt->addUsage(" -h  --help  		Prints this help ");
    opt->addUsage(" -i  --install 	        Installs a package");
    opt->addUsage(" -r  --remove  		Removes a package");
    opt->addUsage(" -s  --sync  		Registers already installed packages");
    opt->addUsage(" -q  --query  		Query a package availability");
    opt->addUsage(" -l  --list 		Lists all packages");
    opt->addUsage("");
    opt->setFlag("help", 'h');
    opt->setOption("install", 'i');
    opt->setOption("remove", 'r');
    opt->setFlag("list", 'l');
    opt->setFlag("sync", 's');
    opt->processCommandArgs(argc, argv);
    if (!opt->hasOptions()) {
        opt->printUsage();
        delete opt;
        return 0;
    }
    if (opt->getFlag("help") || opt->getFlag('h'))
        opt->printUsage();
    if (opt->getValue('i') != NULL || opt->getValue("install") != NULL) {
        string val_(opt->getValue('i'));
        if (query_if_exists(val_)) {
            cout << red << "[Installed] " << def << endl;
        } else {
            string URL = get_git_url(opt->getValue('i'));
            cout << green << "[Getting refs]  " << def << URL.substr(0, URL.length() - 1) << endl;
            bool resuklt = clone(URL.substr(0, URL.length() - 1), opt->getValue('i'));
            cout << endl;
            if (resuklt) {
                cout << green << "[downloading success] " << def << endl;
                insert_into_db((__home__ + _vim_db).c_str(), opt->getValue('i'));
            } else {
                cout << red << "[downloading failed] " << def << endl;
            }
        }
    }
    if (opt->getValue('r') != NULL || opt->getValue("remove") != NULL) {
        string val_(opt->getValue('r'));
        if (query_if_exists(val_)) {
            cout << red << "[REMOVING]  " << def << endl;
            remove_plugin(opt->getValue('r'));
        } else {
            cout << red << "[ERROR]  " << def << "Unable to find in installed packages" << endl;
        }
    }
    if (opt->getFlag("list") || opt->getFlag('l')) {
        cout << green << "[Listing Available Plugins]  " << def << endl;
        query_db();
    }
    if (opt->getFlag("sync") || opt->getFlag('s')) {
        cout << green << "[Syncing]  " << def << endl;
        list_dir();
    }
    return 0;
}