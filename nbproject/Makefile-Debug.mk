#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/440842357/pidk.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/deps/sqlite/sqlite3.o \
	${OBJECTDIR}/_ext/440842357/option.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`pkg-config --libs libcurl` /usr/lib/libgit2.so  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pidk

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pidk: /usr/lib/libgit2.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pidk: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pidk ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/_ext/440842357/pidk.o: /home/piyush/NetBeansProjects/pidk/pidk.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/440842357
	${RM} $@.d
	$(COMPILE.cc) -g -Ideps/libgit2 `pkg-config --cflags libcurl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/440842357/pidk.o /home/piyush/NetBeansProjects/pidk/pidk.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -Ideps/libgit2 `pkg-config --cflags libcurl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/deps/sqlite/sqlite3.o: deps/sqlite/sqlite3.c 
	${MKDIR} -p ${OBJECTDIR}/deps/sqlite
	${RM} $@.d
	$(COMPILE.c) -g `pkg-config --cflags libcurl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/deps/sqlite/sqlite3.o deps/sqlite/sqlite3.c

${OBJECTDIR}/_ext/440842357/option.o: /home/piyush/NetBeansProjects/pidk/option.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/440842357
	${RM} $@.d
	$(COMPILE.cc) -g -Ideps/libgit2 `pkg-config --cflags libcurl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/440842357/option.o /home/piyush/NetBeansProjects/pidk/option.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pidk

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
