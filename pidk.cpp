#include <stdio.h>
#include "pidk.h"

int Ndir_open(Ndir_dir *dir, const char *path) {
    if (dir == NULL || path == NULL || strlen(path) == 0) {
        errno = EINVAL;
        return -1;
    }
    if (strlen(path) + _NDIR_PATH_EXTRA >= _NDIR_PATH_MAX) {
        errno = ENAMETOOLONG;
        return -1;
    }
    dir->_files = NULL;
#ifdef _MSC_VER
    dir->_h = INVALID_HANDLE_VALUE;
#else
    dir->_d = NULL;
#endif
    Ndir_close(dir);
    strcpy(dir->path, path);
#ifdef _MSC_VER
    strcat(dir->path, "\\*");
    dir->_h = FindFirstFile(dir->path, &dir->_f);
    dir->path[strlen(dir->path) - 2] = '\0';
    if (dir->_h == INVALID_HANDLE_VALUE)
#else
    dir->_d = opendir(path);
    if (dir->_d == NULL)
#endif
    {
        errno = ENOENT;
        goto bail;
    }
    dir->has_next = 1;
#ifndef _MSC_VER
    dir->_e = readdir(dir->_d);
    if (dir->_e == NULL) {
        dir->has_next = 0;
    }
#endif
    return 0;

bail:
    Ndir_close(dir);
    return -1;
}

int Ndir_open_sorted(Ndir_dir *dir, const char *path) {
    if (Ndir_open(dir, path) == -1) {
        return -1;
    }
    dir->n_files = 0;
    while (dir->has_next) {
        Ndir_file *p_file;
        dir->n_files++;
        dir->_files = (Ndir_file *) realloc(dir->_files, sizeof (Ndir_file) * dir->n_files);
        if (dir->_files == NULL) {
            errno = ENOMEM;
            goto bail;
        }
        p_file = &dir->_files[dir->n_files - 1];
        if (Ndir_readfile(dir, p_file) == -1) {
            goto bail;
        }
        if (Ndir_next(dir) == -1) {
            goto bail;
        }
    }
    qsort(dir->_files, dir->n_files, sizeof (Ndir_file), Ndir_file_cmp);
    return 0;
bail:
    Ndir_close(dir);
    return -1;
}

void Ndir_close(Ndir_dir *dir) {
    if (dir == NULL) {
        return;
    }
    memset(dir->path, 0, sizeof (dir->path));
    dir->has_next = 0;
    dir->n_files = -1;
    if (dir->_files != NULL) {
        free(dir->_files);
    }
    dir->_files = NULL;
#ifdef _MSC_VER
    if (dir->_h != INVALID_HANDLE_VALUE) {
        FindClose(dir->_h);
    }
    dir->_h = INVALID_HANDLE_VALUE;
#else
    if (dir->_d) {
        closedir(dir->_d);
    }
    dir->_d = NULL;
    dir->_e = NULL;
#endif
}

int Ndir_next(Ndir_dir *dir) {
    if (dir == NULL) {
        errno = EINVAL;
        return -1;
    }
    if (!dir->has_next) {
        errno = ENOENT;
        return -1;
    }

#ifdef _MSC_VER
    if (FindNextFile(dir->_h, &dir->_f) == 0)
#else
    dir->_e = readdir(dir->_d);
    if (dir->_e == NULL)
#endif
    {
        dir->has_next = 0;
#ifdef _MSC_VER
        if (GetLastError() != ERROR_SUCCESS &&
                GetLastError() != ERROR_NO_MORE_FILES) {
            Ndir_close(dir);
            errno = EIO;
            return -1;
        }
#endif
    }

    return 0;
}

int Ndir_readfile(const Ndir_dir *dir, Ndir_file *file) {
    if (dir == NULL || file == NULL) {
        errno = EINVAL;
        return -1;
    }
#ifdef _MSC_VER
    if (dir->_h == INVALID_HANDLE_VALUE)
#else
    if (dir->_e == NULL)
#endif
    {
        errno = ENOENT;
        return -1;
    }
    if (strlen(dir->path) +
            strlen(
#ifdef _MSC_VER
            dir->_f.cFileName
#else
            dir->_e->d_name
#endif
            ) + 1 + _NDIR_PATH_EXTRA >=
            _NDIR_PATH_MAX) {
        errno = ENAMETOOLONG;
        return -1;
    }
    if (strlen(
#ifdef _MSC_VER
            dir->_f.cFileName
#else
            dir->_e->d_name
#endif
            ) >= _NDIR_FILENAME_MAX) {
        errno = ENAMETOOLONG;
        return -1;
    }

    strcpy(file->path, dir->path);
    strcat(file->path, "/");
    strcpy(file->name,
#ifdef _MSC_VER
            dir->_f.cFileName
#else
            dir->_e->d_name
#endif
            );
    strcat(file->path, file->name);
#ifndef _MSC_VER
    if (stat(file->path, &file->_s) == -1) {
        return -1;
    }
#endif
    file->is_dir =
#ifdef _MSC_VER
            !!(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
#else
            S_ISDIR(file->_s.st_mode);
#endif
    file->is_reg =
#ifdef _MSC_VER
            !!(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_NORMAL) ||
            (
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_DEVICE) &&
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_ENCRYPTED) &&
#ifdef FILE_ATTRIBUTE_INTEGRITY_STREAM
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_INTEGRITY_STREAM) &&
#endif
#ifdef FILE_ATTRIBUTE_NO_SCRUB_DATA
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_NO_SCRUB_DATA) &&
#endif
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_OFFLINE) &&
            !(dir->_f.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY));
#else
            S_ISREG(file->_s.st_mode);
#endif

    return 0;
}

int Ndir_readfile_n(const Ndir_dir *dir, Ndir_file *file, int i) {
    if (dir == NULL || file == NULL || i < 0) {
        errno = EINVAL;
        return -1;
    }
    if (i >= dir->n_files) {
        errno = ENOENT;
        return -1;
    }

    memcpy(file, &dir->_files[i], sizeof (Ndir_file));

    return 0;
}

int Ndir_open_subdir_n(Ndir_dir *dir, int i) {
    char path[_NDIR_PATH_MAX];
    if (dir == NULL || i < 0) {
        errno = EINVAL;
        return -1;
    }
    if (i >= dir->n_files || !dir->_files[i].is_dir) {
        errno = ENOENT;
        return -1;
    }

    strcpy(path, dir->_files[i].path);
    Ndir_close(dir);
    if (Ndir_open_sorted(dir, path) == -1) {
        return -1;
    }

    return 0;
}

int Ndir_file_cmp(const void *a, const void *b) {
    const Ndir_file *fa = (const Ndir_file *) a;
    const Ndir_file *fb = (const Ndir_file *) b;
    if (fa->is_dir != fb->is_dir) {
        return -(fa->is_dir - fb->is_dir);
    }
    return strncasecmp(fa->name, fb->name, _NDIR_FILENAME_MAX);
}

