/* 
 * File:   config.h
 * Author: piyush
 *
 * Created on 8 July, 2013, 11:02 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#define _vim_db ".vim/bundle/.vim_modules.config"
#define _downloads ".vim/bundle/"
#define _Api_url "http://localhost/"

#endif	/* CONFIG_H */

