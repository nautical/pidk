#include <errno.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#pragma warning (disable : 4996)
#else
#include <dirent.h>
#include <sys/stat.h>
#endif

#define _NDIR_PATH_MAX 4096
#ifdef _MSC_VER
/* extra chars for the "\\*" mask */
#define _NDIR_PATH_EXTRA 2
#else
#define _NDIR_PATH_EXTRA 0
#endif
#define _NDIR_FILENAME_MAX 256

#ifdef _MSC_VER
#define strncasecmp _strnicmp
#endif

typedef struct {
    char path[_NDIR_PATH_MAX];
    char name[_NDIR_FILENAME_MAX];
    int is_dir;
    int is_reg;

#ifdef _MSC_VER
#else
    struct stat _s;
#endif
} Ndir_file;

typedef struct {
    char path[_NDIR_PATH_MAX];
    int has_next;
    int n_files;

    Ndir_file *_files;
#ifdef _MSC_VER
    HANDLE _h;
    WIN32_FIND_DATA _f;
#else
    DIR *_d;
    struct dirent *_e;
#endif
} Ndir_dir;


int Ndir_open(Ndir_dir *dir, const char *path);
int Ndir_open_sorted(Ndir_dir *dir, const char *path);
void Ndir_close(Ndir_dir *dir);
int Ndir_next(Ndir_dir *dir);
int Ndir_readfile(const Ndir_dir *dir, Ndir_file *file);
int Ndir_readfile_n(const Ndir_dir *dir, Ndir_file *file, int i);
int Ndir_open_subdir_n(Ndir_dir *dir, int i);
int Ndir_file_cmp(const void *a, const void *b);